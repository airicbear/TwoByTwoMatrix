
public class TwoByTwoMatrix {
	private double topLeft, topRight, botLeft, botRight;
	
	/**
	 * Creates 2x2 zero matrix
	 */
	public TwoByTwoMatrix()
	{
		setMatrix(0);
	}
	
	/**
	 * Creates 2x2 matrix
	 * @param topLeft a
	 * @param topRight b
	 * @param botLeft c
	 * @param botRight d
	 */
	public TwoByTwoMatrix(double topLeft, double topRight, double botLeft, double botRight)
	{
		setMatrix(topLeft, topRight, botLeft, botRight);
	}
	
	/**
	 * Creates 2x2 matrix with all values set to num
	 * @param num double
	 */
	public TwoByTwoMatrix(double num)
	{
		setMatrix(num);
	}
	
	/**
	 * Creates 2x2 matrix with diagonals set to 1
	 * @param topRight b
	 * @param botLeft c
	 */
	public TwoByTwoMatrix(double topRight, double botLeft)
	{
		setMatrix(1, topRight, botLeft, 1);
	}
	
	public TwoByTwoMatrix(String string)
	{
		if(string == "random")
		{
			setMatrix(randomNumber(), randomNumber(), randomNumber(), randomNumber());
		}
	}

	public String toString()
	{
		return "[ " + topLeft + ", " + topRight + " ]" + "\n"
			+  "[ " + botLeft + ", " + botRight + " ]" + "\n";
	}
	
	@Override
	public boolean equals(Object obj)
	{
		return	this.topLeft==((TwoByTwoMatrix)obj).topLeft &&
				this.topRight==((TwoByTwoMatrix)obj).topRight &&
				this.botLeft==((TwoByTwoMatrix)obj).botLeft &&
				this.botRight==((TwoByTwoMatrix)obj).botRight;
	}
	
	/** 
	 * Determinant of the matrix
	 * @return determinant
	 */
	public double determinant()
	{
		double determinant = (topLeft * botRight) - (botLeft * topRight);
		return determinant;
	}
	
	/**
	 *	[ a b ] * [ e f ] = [(ac + bg)  (af + bh)]
	 *	<br>
	 *	[ c d ] &nbsp  [ g h ]  &nbsp&nbsp&nbsp [(ce + dg)  (cf + dh)]
	 * @param other The other matrix which this one will be multiplied by
	 * @return The product of this matrix and the other matrix
	 */
	public TwoByTwoMatrix multiply(TwoByTwoMatrix other)
	{

		double a = this.topLeft;
		double b = this.topRight;
		double c = this.botLeft;
		double d = this.botRight;
		double e = other.topLeft;
		double f = other.topRight;
		double g = other.botLeft;
		double h = other.botRight;
		TwoByTwoMatrix result = new TwoByTwoMatrix(
				a * e + b * g,
				a * f + b * h,
				c * e + d * g,
				c * f + d * h);
		return result;
	}
	

	public TwoByTwoMatrix times(double scalar)
	{
		return new TwoByTwoMatrix(
				this.topLeft * scalar,
				this.topRight * scalar,
				this.botLeft * scalar,
				this.botRight * scalar);
	}
	
	public TwoByTwoMatrix inverse()
	{
		if(this.determinant() == 0)
		{
			return null;
		}
		TwoByTwoMatrix result = new TwoByTwoMatrix(
				this.botRight,
				-this.topRight,
				-this.botLeft,
				this.topLeft);
		return result.times(1/this.determinant());
	}
	
	public void setMatrix(double num)
	{
		setMatrix(num, num, num, num);
	}
	
	public void setMatrix(double topLeft, double topRight, double botLeft, double botRight)
	{
		setTopLeft(topLeft);
		setTopRight(topRight);
		setBotLeft(botLeft);
		setBotRight(botRight);
	}
	
	public void setTopLeft(double topLeft)
	{
		this.topLeft = topLeft;
	}
	
	public void setTopRight(double topRight)
	{
		this.topRight = topRight;
	}
	
	public void setBotLeft(double botLeft)
	{
		this.botLeft = botLeft;
	}

	public void setBotRight(double botRight) {
		this.botRight = botRight;
	}

	public double getBotRight() {
		return botRight;
	}
	
	public double getTopLeft() {
		return topLeft;
	}

	public double getTopRight() {
		return topRight;
	}

	public double getBotLeft() {
		return botLeft;
	}
	
	private double randomNumber()
	{
		return Math.round(Math.random() * 10);
	}
}
