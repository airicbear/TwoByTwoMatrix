
public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		TwoByTwoMatrix matrix = new TwoByTwoMatrix(1,2,3,4);
		TwoByTwoMatrix inverse = new TwoByTwoMatrix(-2, 1, 1.5, -0.5);
		System.out.println(matrix.inverse());
		System.out.println(inverse);
		System.out.println(matrix.inverse().equals(inverse));
	}
}